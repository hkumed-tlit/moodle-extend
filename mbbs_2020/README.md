Theme:	Boost / BoostCampus
Course Format:	Flexible Sections Format
Create HTML Block:  (MBBS 2020 for example)
<script src="https://tl.med.hku.hk/moodle-extend/base/js/base.js"></script>
<script src="https://tl.med.hku.hk/moodle-extend/base/js/admin.js"></script>
<script src="https://tl.med.hku.hk/moodle-extend/mbbs_2020/js/custom.js"></script>
<script>
includeCSS('https://tl.med.hku.hk/moodle-extend/base/css/base.css');
includeCSS('https://tl.med.hku.hk/moodle-extend/mbbs_2020/css/cmap.css');
includeCSS('https://tl.med.hku.hk/moodle-extend/mbbs_2020/css/custom.css');
</script>
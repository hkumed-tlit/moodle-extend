/*
<fusedoc>
	<description>
		Extending the features of Enlightlite theme:
		- includeCSS
	</description>
</fusedoc>
*/


// load css file
function includeCSS(src) {
	var link = document.createElement('link');
	link.rel = 'stylesheet';
	link.href = src;
	document.head.appendChild(link);
}